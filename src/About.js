import React from 'react'

const About = () => {
  return (
    <div>
       {/* ======= About Section ======= */}
  <section id="about" className="about">
    <div className="container" data-aos="fade-up">

      <div className="row">
        <div className="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
          <img src="assets/img/about.jpg" className="img-fluid" alt=""/>
        </div>
        <div className="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
          <h3>About Fitness Always</h3>
          <p className="fst-italic">
          Fitness Always is Asia’s top fitness organization. It has the most number of authentic World Champions available anywhere in the world. Our award-winning instructors are not only proven competitors at the highest levels in the world, but they have also earned instructor certifications at the highest 
levels from the foremost authorities. Fitness Always consistently ranks among the best fitness organizations in the world for fitness.
By creating a safe, fun, and supportive environment, Fitness Always enables everyone to enjoy the benefits of fitness:
          </p>
          <ul>
            <li><i className="ri-check-double-line"></i> Fitness and good health</li>
            <li><i className="ri-check-double-line"></i> Confidence, mental strength, and discipline</li>
            <li><i className="ri-check-double-line"></i> Lifelong friendships</li>
          </ul>
          {/* <p>
            Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident
          </p> */}
        </div>
      </div>

    </div>
  </section>{/* End About Section */}

    </div>
  )
}

export default About
