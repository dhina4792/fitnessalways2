import React, { useState, useEffect } from "react";
import axios from "axios";

const options = {
  method: "GET",
  url: "https://keto-diet.p.rapidapi.com/",
  headers: {
    "X-RapidAPI-Key": "473d2e2d80mshdf3d12cdc1a2fccp1b2e89jsnbd730ce1a2cf",
    "X-RapidAPI-Host": "keto-diet.p.rapidapi.com",
  },
};
const Products = () => {
  //const [products, setProducts] = useState(prodData);
  const [products, setProducts] = useState([]);
  const randomValue = 10;
  //TODO: randomise randomValue
  //const [movieTitle, setMovieTitle] = useState('');
  const ketoRecipes = async () => {
    const response = await axios.request(options);
    setProducts(response.data);
  };


  useEffect(() => {
    ketoRecipes();
  }, []);
  let randomReceipes = [...Array(20)].map(_ => Math.random() * products.length | 0)
  console.log(randomReceipes);
  return (
    <div>
      {/* ======= Portfolio Section ======= */}
      <section id="portfolio" className="portfolio">
        <div className="container" data-aos="fade-up">
          <div className="section-title">
            <h2>Recipies</h2>
            <p>Check our Keto Free Recipes</p>
          </div>

          {/* <div className="row" data-aos="fade-up" data-aos-delay="100">
        <div className="col-lg-12 d-flex justify-content-center">
          <ul id="portfolio-flters">
            <li data-filter="*" className="filter-active">All</li>
            <li data-filter=".filter-app">App</li>
            <li data-filter=".filter-card">Card</li>
            <li data-filter=".filter-web">Web</li>
          </ul>
        </div>
      </div> */}

          <div
            className="row portfolio-container"
            data-aos="fade-up"
            data-aos-delay="200"
          >
            {products.filter((productList) => randomReceipes.includes(productList.id)).map((filteredPoductList) => {

              return (
                <div className="col-lg-4 col-md-6 portfolio-item filter-app">
                  <div className="portfolio-wrap">
                    <img src={filteredPoductList.image} className="img-fluid" alt="" />
                    <div className="portfolio-info">
                      <h4>{filteredPoductList.recipe}</h4>

                      <p> {filteredPoductList.directions_step_1}</p>
                      <p>{filteredPoductList.directions_step_2}</p>
                      <p>{filteredPoductList.directions_step_3}</p>
                      <p>{filteredPoductList.directions_step_4}</p>
                      <div className="portfolio-links">
                        <a
                          href={filteredPoductList.image}
                          data-gallery="portfolioGallery"
                          className="portfolio-lightbox"
                          title="App 1"
                        >
                          <i className="bx bx-plus"></i>
                        </a>
                        <a href={filteredPoductList.image} title="More Details">
                          <i className="bx bx-link"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Products;
